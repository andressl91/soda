#include <string>
#include <iostream>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <cstdlib>
#include <string>

#include "soda/protocol/dtss_ws.h"
#include "soda/server/client_ws.h"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>



int main() {

    const char * request_id = "3";
    const bool merge_store = false;
    const bool recreate_ts = true;
    const bool cache = true; 

    StoreTemplate st(request_id, merge_store, recreate_ts, cache);

    //const char * id = R"_(shyft://foo/a2.db)_";
    const char * id = "shyft://foo/a1.db";
    bool point_fx = true;
    const int dt = 3600;
    const int n = 3;
    const char * t0 = "2018-01-01T00:00:00Z";
    int val[] = {2,6,9};
    st.add_ts(id, point_fx,
            t0, dt, n, val);
    
    auto dc = st.get_doc();
    std::string s;
    //serializeJson(dc, s);
    s = st.get_ws_store_ts();
    std::cout << "\n";
    std::cout << s << std::endl;
    std::cout << "\n";

    //std::string store = R"(store_ts {"request_id":"3","merge_store":true,"recreate_ts":true,"cache":true,"tsv":[{"id":"shyft://foo/a1.db","pfx":true,"time_axis":{"t0":"2018-01-01T00:00:00Z","dt":3600,"n":3},"values":[1,2,3]}]})";
    //send_on_ws(store);
    DtssWsClient client("0.0.0.0", "4444");
    client.send_on_ws(s);

    return 0;
}

