#include "soda/server/client.h"
#include <iostream>

void start_client() {
    int b = 2222;
    const char * host = "0.0.0.0";
    SodaClient c(host, b);
    const char * msg = "{\"Temperature\":\"10\",\"Humidity\":\"20\",\"Status\":\"0\"}\n";
    c.send_message(msg);
}

int main() {
    start_client();
}
