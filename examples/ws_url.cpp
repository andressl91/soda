using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
using boost::system::error_code;


 class pub_sub_session : public std::enable_shared_from_this<pub_sub_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        bool waiting_first_read=true;
        // Report a failure
        void
        fail(error_code ec, char const* what) {
            fail_= string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec,diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit
        pub_sub_session(boost::asio::io_context& ioc):resolver_(ioc),ws_(ioc){}
        vector<string> responses_;
        string diagnostics() const {return fail_;}

        // Start the asynchronous operation
        void
        run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_,port_,// Look up the domain name
                [me=shared_from_this()](error_code ec,tcp::resolver::results_type results) {
                    me->on_resolve(ec,results);
                }
            );
        }

        void
        on_resolve(error_code ec,tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(),results.begin(),results.end(),
                // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
                std::bind(&pub_sub_session::on_connect,shared_from_this(),std::placeholders::_1)
            );
        }

        void
        on_connect(error_code ec){
            fail_on_error(ec, "connect")
            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                [me=shared_from_this()](error_code ec) {me->store_first_ts(ec);}
            );
        }

        void
        store_first_ts(error_code ec) {
            fail_on_error(ec, "store_first_ts")
            ws_.async_write( // start storing the first time-series
                boost::asio::buffer(
                    R"_(store_ts {
                        "request_id"  : "1w",
                        "merge_store" : false,
                        "recreate_ts" : false,
                        "cache"       : true,
                        "tsv"         : [
                                        {
                                            "id": "shyft://test/a1",
                                            "pfx":true,
                                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                            "values": [1,2,3]
                                        }
                                        ,
                                        {
                                            "id": "shyft://test/a2",
                                            "pfx":false,
                                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                            "values": [4,5,6]
                                        }
                                    ]
                    })_"),
                [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                    me->send_read_and_subscribe(ec,bytes_transferred);
                }
            );
            ws_.async_read(buffer_, // and also start read incoming messages
                [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                    me->on_read(ec,bytes_transferred);
                }
            );
        }

