import shyft.time_series as sa

cal = sa.Calendar()

t_now = sa.utctime_now()
t_start = cal.trim(t_now, cal.DAY)
ta = sa.TimeAxis(t_start, cal.HOUR, 3)
print(int(t_start))


ts = sa.TimeSeries("shyft://foo/a1.db")
tsv = sa.TsVector()
tsv.append(ts)

dtsc = sa.DtsClient("0.0.0.0:2222")

tsv_get = dtsc.evaluate(tsv, ta.total_period())
print(tsv_get[0].v.to_numpy())
print(tsv_get[0](t_start + cal.HOUR - cal.MINUTE*30))

