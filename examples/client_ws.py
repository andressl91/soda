import websockets
import asyncio
#msg = r"""store_ts {
#        "request_id":"3",
#        "merge_store":false,
#        "recreate_ts":false,
#        "cache":true,
#        "tsv":[{
#                            "id":"shyft://foo/a1.db",
#                            "pfx":true,
#                            "time_axis":{"t0":"2018-01-01T00:00:00Z","dt": 3600,"n":3},
#                            "values":[1,2,3]
#                        }]}"""

msg = r"""store_ts {
        "request_id":"3",
        "merge_store":true,
        "recreate_ts":false,
        "cache":true,
        "tsv":[{
                            "id":"shyft://foo/a1.db",
                            "pfx":true,
                            "time_axis":{"t0":"2018-01-01T00:00:00Z","dt": 3600,"n":3},
                            "values":[1,2,3]
                        }]}"""

async def hello():
    uri = "ws://0.0.0.0:4444"

    async with websockets.connect(uri) as websocket:
        await websocket.send(msg)
        foo = await websocket.recv()
        print(foo)
asyncio.get_event_loop().run_until_complete(hello())







