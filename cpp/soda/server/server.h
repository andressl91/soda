#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include "ArduinoJson.h"

using namespace boost::asio;  
using ip::tcp;  
struct SodaServer {
    private:
        int port;

    public:
    SodaServer(int port) : port{port} {}    

    void process_json(std::string json_string, const int buffer_size) {
    
        //TODO: Calculate capacity, can std::size_t from read_ be used ??
        //const int capacity = JSON_OBJECT_SIZE(3);
        //StaticJsonDocument<buffer_size + 1> doc;
        //StaticJsonDocument<buffer_size> doc;
        StaticJsonDocument<1000> doc;
        DeserializationError err = deserializeJson(doc, json_string);

        if (err) {
        std::cout << "failed at life \n";
        std::cout << "Deserialize failed with: " << err.c_str() << "\n";
        }
        else {
            const char* temp = doc["Temperature"];
            std::cout << temp << std::endl;
            std::cout << temp << std::endl;
            std::cout << "Json Temperature is: " << temp << std::endl;
        }
    }

    std::string read_(tcp::socket & socket) {  
           boost::asio::streambuf buf;  

           std::size_t n = read_until(socket, buf, "}\n");
           std::cout << "Size_t is: " << n << std::endl;
           //  ---- Convert buffer to string -----
           // Method 1
           //std::string json_stream = boost::asio::buffer_cast<const char*>(buf.data());  

           // Method 2
           streambuf::const_buffers_type bufs = buf.data();
           std::string json_stream(
           buffers_begin(bufs),
           buffers_begin(bufs) + n);

           //TODO: debugflag
           std::cout << json_stream << std::endl;
           //TODO:: Do verification that we got json...
           std::cout << "Sending recieved string to json\n";
           process_json(json_stream, n);
           
           return json_stream;  
    }
    int get_port() { return port; }

    void send_(tcp::socket & socket, const std::string& message) {  
           const std::string msg = message + "\n";  
           boost::asio::write(socket, boost::asio::buffer(message) );  
    }
    int start_io() {
        try {
            boost::asio::io_service io_service;

            //listen for new connection
            tcp::acceptor acceptor_(io_service, tcp::endpoint(tcp::v4(), port));


            for(;;) {
                //socket creation
                tcp::socket socket_(io_service);

                //waiting for the connection
                acceptor_.accept(socket_);


                //read operation
                std::string message = read_(socket_);

                //write operation
                //send_(socket_, "Hello From Server!");
                //std::cout << "Servent sent Hello message to Client!" << std::endl;
            }
          }
        catch(std::exception& e) {
            std::cerr << e.what() << std::endl;

        }
          return 0;
        }
    };
