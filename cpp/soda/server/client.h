#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>


using namespace boost::asio;  
using ip::tcp;  
struct SodaClient {
    private:
    const char * host;
    int port;
    boost::asio::io_service io_service;
    tcp::socket socket;
    public:
    SodaClient(const char * host, int port) : port{port}, host{host},
                                              socket{io_service} {}

    void connect() {
        socket.connect(tcp::endpoint(boost::asio::ip::address::from_string(host), port));
    }

    int send_message(std::string msg) {
        connect();

        boost::system::error_code error;
        boost::asio::write(socket, boost::asio::buffer(msg), error);

        if( !error ) {
           std::cout << "Client sent hello message!" << std::endl;
        }
        else {
           std::cout << "send failed: " << error.message() << std::endl;
        }
            //getting response from server
            boost::asio::streambuf receive_buffer;
            boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
            if( error && error != boost::asio::error::eof ) {
                std::cout << "receive failed: " << error.message() << std::endl;
            }
            else {
                const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
                std::cout << data << std::endl;
            }
            return 0;
    }
};
