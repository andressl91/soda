// TODO: Only for debugginh
#include <iostream> 
#include "ArduinoJson.h"

const int json_capacity = JSON_ARRAY_SIZE(2) + 2*JSON_ARRAY_SIZE(3) + 
           2*JSON_OBJECT_SIZE(3) + 2*JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5);
struct StoreTemplate {

    private:
        const char * request_id;
        const bool merge_store;
        const bool recreate_ts;
        const bool cache;
        //TODO: Solve the json_capacity setting, something else than glob json_capacity
        //size_t json_capacity;
        DynamicJsonDocument doc;

    public:
        StoreTemplate(const char * request_id,
                      const bool merge_store,
                      const bool recreate_ts,
                      const bool cache) : request_id{request_id}, merge_store{merge_store},
                                          recreate_ts{recreate_ts}, cache(cache), 
                                          doc{json_capacity} {
    
                                            create_skeleton();
                                          }  
        
        DynamicJsonDocument get_doc() {
            return doc;
        }

        bool get_cache() {
            return cache;
        }
        void create_skeleton() {
            doc["request_id"] = request_id;
            doc["merge_store"] = merge_store;
            doc["recreate_ts"] = recreate_ts;
            doc["cache"] = cache;
        }

        void add_values(JsonArray * j_array, const char * values) {
            //TODO: Loop over values and add to j_array
            const char *val_start = values;
            std::cout << "In while loop \n";
            j_array->add(*val_start);
            //j_array.add((*values));
            //while(*values != 0x00) {

            //    std::cout << "In while loop \n";
            //    if(*values == ' ' || values == ";"){
            //        std::cout << "Adding values\n";
            //        j_array.add((*values - 1));
            //    }
            //    ++values;
            //}
        }

        void add_ts(const char * dtss_url,
                    const bool point_fx,
                    const char * t0,
                    const int dt,
                    const int n,
                    const int * x) {

            //NOTE: point_fx=false == INSTANT_VALUE
            JsonArray tsv = doc.createNestedArray("tsv");
            JsonObject tsv_0 = tsv.createNestedObject();
            tsv_0["id"] = dtss_url;
            tsv_0["pfx"] = true;

            JsonObject tsv_0_time_axis = tsv_0.createNestedObject("time_axis");
            //tsv_0_time_axis["t0"] = "2018-01-01T00:00:00Z";
            tsv_0_time_axis["t0"] = t0;
            tsv_0_time_axis["dt"] = dt;
            tsv_0_time_axis["n"] = n;

            JsonArray tsv_0_values = tsv_0.createNestedArray("values");
            for(int i = 0; i <= sizeof(x)/sizeof(int) ; ++i) {
                tsv_0_values.add(x[i]);
        }
        }
        void add_ts(const char * dtss_url,
                    const bool point_fx,
                    const char * t0,
                    const int dt,
                    const int n,
                    const double * x) {

            //NOTE: point_fx=false == INSTANT_VALUE
            JsonArray tsv = doc.createNestedArray("tsv");
            JsonObject tsv_0 = tsv.createNestedObject();
            tsv_0["id"] = dtss_url;
            tsv_0["pfx"] = true;

            JsonObject tsv_0_time_axis = tsv_0.createNestedObject("time_axis");
            //tsv_0_time_axis["t0"] = "2018-01-01T00:00:00Z";
            tsv_0_time_axis["t0"] = t0;
            tsv_0_time_axis["dt"] = dt;
            tsv_0_time_axis["n"] = n;

            JsonArray tsv_0_values = tsv_0.createNestedArray("values");
            for(int i = 0; i <= sizeof(x)/sizeof(double) ; ++i) {
                tsv_0_values.add(x[i]);
        }
        }

        std::string get_ws_store_ts() {
            std::string output = "store_ts ";
            serializeJson(doc, output);
            return output;
        }
};

struct SensorStore : StoreTemplate {
};

