#include "ArduinoJson.h"
//Works
//const int capacity = 2;
//StaticJsonDocument<capacity> doc;
struct Protocol {
    const int capacity;
    StaticJsonDocument<100> doc;
    //TODO: Set capacity in constructor
    //StaticJsonDocument<capacity> doc;
    Protocol(const int capacity) : capacity(capacity) {}
    template <typename T>
    void add(const char * key, T value) {
        doc[key] = value;
    }
    JsonObject get_doc_ref() {
        return doc.as<JsonObject>();
    }


};


struct WeatherDataProtocol : public Protocol {
    WeatherDataProtocol(const int capacity): Protocol(capacity) {}
};

struct VerifyWeatherDataProtocol {
    WeatherDataProtocol wdp;
    VerifyWeatherDataProtocol(WeatherDataProtocol wdp) 
        : wdp(wdp) {}
    bool verify_protocol() {
        JsonObject doc_ref = wdp.get_doc_ref();

        if(doc_ref.containsKey("temp")) {
        JsonVariant tmp = doc_ref["temp"];
            if(!tmp.is<float>())
                return false; 
        }
        else 
            return false;

        if(doc_ref.containsKey("time")) {
            JsonVariant stamp = doc_ref["time"];
            if(!stamp.is<int>())
                return false; 
        }
        else 
            return false;
        return true; 
    }
};
