#include <iostream>
#include "doctest/doctest.h"
#include "ArduinoJson.h"
#include "soda/server/server.h"

TEST_SUITE("server"){
    TEST_CASE("server base"){
        int port = 20002;
        SodaServer sodaserver(port);
        CHECK(sodaserver.get_port() == port);
    }
}
