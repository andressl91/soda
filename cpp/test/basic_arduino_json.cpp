#include <iostream>
#include "ArduinoJson.h"
#include "doctest/doctest.h"
#include "soda/protocol/protocol.h"

int foo(int x) {
    return x;
}
TEST_SUITE("protocol"){
    TEST_CASE("protocol base") {
    
    Protocol proto(10);
    CHECK(proto.capacity == 10);
    }

    TEST_CASE("protocol weather") {
        WeatherDataProtocol wdp(20);
        CHECK(wdp.capacity == 20);
        wdp.add("time", 100000);
        wdp.add("temp", 10.5);
        //TODO: ADD MEMBER ACCORDING TO VERIFYWEATHERDATAPROTOCOL

        JsonObject doc_ref = wdp.get_doc_ref();
        JsonVariant tmp = doc_ref["temp"];
        
        // Check type is right
        CHECK(tmp.is<float>());
        // Explicit cast anc check value
        auto t = tmp.as<float>();
        CHECK(t == 10.5);
        SUBCASE("protocol verify weather") {
            VerifyWeatherDataProtocol vwdp(wdp);
            CHECK(vwdp.verify_protocol());
        }
    }
}
