#include <string>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <cstdlib>
#include "ArduinoJson.h"
#include "doctest/doctest.h"
#include "soda/protocol/dtss_ws.h"

TEST_SUITE("dtss_ws"){
    TEST_CASE("dtss_ws skeleton") {
     
        const char * request_id = "1w";
        const bool merge_store = true;
        const bool recreate_ts = true;
        const bool cache = true;

        StoreTemplate temp(request_id, merge_store, recreate_ts, cache);

        DynamicJsonDocument doc = temp.get_doc();
        JsonObject obj_ref = doc.as<JsonObject>();

        SUBCASE("dtss_ws check expected skeleton") {
            JsonVariant request_id_p = obj_ref["request_id"];
            CHECK(obj_ref.containsKey("request_id") == true);
            CHECK(request_id_p.is<char*>() == true);
            auto l1 = request_id_p.as<char *>();
            CHECK(l1 == "1w");
        

            JsonVariant merge_store_p = obj_ref["merge_store"];
            CHECK(obj_ref.containsKey("merge_store") == true);
            CHECK(merge_store_p.is<bool>() == true);
            auto l2 = merge_store_p.as<bool>();
            CHECK(l2 == true);

            JsonVariant recreate_ts_p = obj_ref["recreate_ts"];
            CHECK(obj_ref.containsKey("recreate_ts") == true);
            CHECK(recreate_ts_p.is<bool>() == true);
            auto l3 = recreate_ts_p.as<bool>();
            CHECK(l3 == true);

            JsonVariant cache_p = obj_ref["cache"];
            CHECK(obj_ref.containsKey("cache") == true);
            CHECK(cache_p.is<bool>() == true);
            auto l4 = cache_p.as<bool>();
            CHECK(l4 == true);
            
            std::string expected = "{\"request_id\":\"1w\",\"merge_store\":true,\"recreate_ts\":true,\"cache\":true}";
            std::string foo;
            serializeJson(doc, foo);
            CHECK(foo == expected);
            }

        // ADD TimeSeries 
        const char * dtss_url = "shyft://foo/bar.db";
        bool point_fx = true;
        const int dt_in = 3600;
        const int n_in = 4;
        const char * t0_in = "2018-01-01T00:00:00Z";
        const int v_in[] = {2,6,9};
        temp.add_ts(dtss_url, point_fx, t0_in, dt_in, n_in, v_in);

        doc = temp.get_doc();
        obj_ref = doc.as<JsonObject>();

        CHECK(obj_ref.containsKey("tsv") == true);
        JsonObject tsv_0 = doc["tsv"][0];

        SUBCASE("dtss_ws add timeseries") {

            SUBCASE("dtss_ws tsv identifier point_interpretation") {

            CHECK(tsv_0.containsKey("id") == true);
            CHECK(tsv_0["id"].is<char*>() == true);
            auto id1= tsv_0["id"].as<char*>();
            CHECK(id1 == dtss_url);
            CHECK(id1 == "shyft://foo/bar.db");
            CHECK(tsv_0.containsKey("pfx") == true);
            CHECK(tsv_0["pfx"].is<bool>() == true);
            auto pfx = tsv_0["pfx"].as<bool>();
            CHECK(pfx == point_fx);
        }

            SUBCASE("dtss_ws tsv time_axis") {
            CHECK(tsv_0.containsKey("time_axis") == true);
            JsonObject tsv_0_time_axis = tsv_0["time_axis"];

            CHECK(tsv_0_time_axis.containsKey("t0") == true);
            CHECK(tsv_0_time_axis["t0"].is<char*>() == true);
            //TODO: casting to char gives error in compare, something with ":"
            //auto t0= tsv_0_time_axis["t0"].as<char*>();
            std::string t0= tsv_0_time_axis["t0"].as<std::string>();
            CHECK(t0 == t0_in);

            CHECK(tsv_0_time_axis.containsKey("dt") == true);
            CHECK(tsv_0_time_axis["dt"].is<int>() == true);
            int dt = tsv_0_time_axis["dt"].as<int>();
            CHECK(dt == dt_in);

            CHECK(tsv_0_time_axis.containsKey("n") == true);
            CHECK(tsv_0_time_axis["n"].is<int>() == true);
            int n = tsv_0_time_axis["n"].as<int>();
            CHECK(n == n_in);
            }
        //TODO: ------------------- IMPORTANT --------------------------------
        //THE FOLLOWING VALUES IN TIME_AXIS, VALUES ARE HARDCODED INTO SOURCECODE FOR TESTING FOR NOW
            SUBCASE("dtss_ws tsv values") {
                CHECK(tsv_0.containsKey("values"));
                JsonObject tsv_0_values = tsv_0["values"];
                JsonArray foo = tsv_0["values"];
                //CHECK(foo[0] == "1");
                CHECK(foo[0] == v_in[0]);
                CHECK(foo[1] == v_in[1]);
                CHECK(foo[2] == v_in[2]);
                }
        }

    }
}
