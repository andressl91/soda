PROJECT_WORKSPACE=${SHYFT_DEPENDENCIES:=$(readlink --canonicalize --no-newline `dirname ${0}`/../)}
PROJECT_DEPENDENCIES=$PROJECT_WORKSPACE/dependencies

cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER"
boost_ver=1_71_0

echo ${PROJECT_DEPENDENCIES}
cd ${PROJECT_DEPENDENCIES}
if [ ! -d doctest ]; then
    echo Building doctest
    git clone https://github.com/onqtam/doctest
    pushd doctest
    cmake . -DCMAKE_INSTALL_PREFIX="${PROJECT_DEPENDENCIES}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
    make install -j4
    popd
fi;
echo Done doctest

echo Fetching ArduinoJson headerfile
if [ ! -f ${PROJECT_DEPENDENCIES}/include/ArduinoJson.h ]; then
    wget -O ${PROJECT_DEPENDENCIES}/include/ArduinoJson.h https://github.com/bblanchon/ArduinoJson/releases/download/v6.13.0/ArduinoJson-v6.13.0.h
fi;
echo Done ArduinoJson

if [ ! -d boost_${boost_ver} ]; then
    echo Building boost_${boost_ver}
    if [ ! -f boost_${boost_ver}.tar.gz ]; then
        wget -O boost_${boost_ver}.tar.gz https://dl.bintray.com/boostorg/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
    fi;
    tar -xf boost_${boost_ver}.tar.gz
    pushd boost_${boost_ver}
    ./bootstrap.sh --prefix="${PROJECT_DEPENDENCIES}"
    #py_root=${SHYFT_WORKSPACE} # here we could tweak using virtual-env, conda or system.. match with cmake
    # have to help boost figure out right python versions bin,inc and libs.
    # first remove any python that was found with the bootstrap step above
    #mv -f project-config.jam x.jam
    #cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
    #echo "using python : 3.8 : ${py_root}/shyft_38/bin/python : ${py_root}/shyft_38/include/python3.8m : ${py_root}/shyft_38/lib ;" >>project-config.jam

    boost_packages="--with-system --with-thread --with-date_time --with-regex --with-serialization"
    #boost_packages="--with-system --with-filesystem --with-date_time --with-python --with-serialization python=3.7"
 #   fi;

    #./b2 -j4 -d0 link=shared variant=release threading=multi ${boost_packages}
    ./b2 -j4 -d0 install link=shared variant=release threading=multi ${boost_packages}
    popd
fi;



