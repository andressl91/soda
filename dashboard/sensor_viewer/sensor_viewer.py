import itertools
from typing import Optional, Dict, Any, List
from concurrent.futures import ThreadPoolExecutor

from shyft.dashboard.time_series.ds_view_handle_registry import DsViewHandleRegistryApp
from shyft.dashboard.time_series.renderer import LineRenderer, DiamondScatterRenderer
from shyft.dashboard.time_series.view_container.figure import Figure

from shyft.dashboard.time_series.ts_viewer import TsViewer

from shyft.dashboard.base.selector_presenter import SelectorPresenter
from shyft.dashboard.base.selector_views import FilterMultiSelect

from shyft.dashboard.base.app import AppBase

from dashboard.sensor_viewer.widgets import (ConsolePrinter,
                                             ContainerPathReceiver,
                                             TsSelector,
                                             DsView,
                                            LineStyleChanger)
import bokeh
import bokeh.layouts
import bokeh.models

from shyft.dashboard.base.ports import connect_ports, Receiver
from shyft.dashboard.time_series.view_container.table import Table
from shyft.dashboard.widgets.logger_box import LoggerBox
from shyft.dashboard.widgets.message_viewer import MessageViewer

import shyft.time_series as sa


class SensorDashboard(AppBase):

    def __init__(self, thread_pool: Optional[ThreadPoolExecutor]=None,
                 app_kwargs: Optional[Dict[str, Any]]=None) -> None:
        super().__init__(thread_pool=thread_pool)
    @property
    def name(self) -> str:
        """
        This property returns the name of the app
        """
        #return "MY App"
        return "sensor dashboard"

    def get_layout(self, doc: 'bokeh.document.Document', logger: Optional[LoggerBox]=None) -> bokeh.models.LayoutDOM:
        """
        This function returns the full page layout for the app
        """
        # Message board, handy for logging actions/callbacks results to user
        event_messenger = MessageViewer(title='Log:', rows=10, width=2000,
                                        height=200,
                                        title_hight=20, sizing_mode=None, show_time=True,
                                        time_zone='Europe/Oslo')
        container_path_input = ContainerPathReceiver()
        # Connect dtss-container userinput to message board
        connect_ports(container_path_input.send_dtss_url,
                      event_messenger.receive_message)
        # Connect dtss-url  userinput to message board
        connect_ports(container_path_input.send_event_message,
                      event_messenger.receive_message)

        # Multi selector for timeseries found in container
        # Define the view (window), with appropriate sizes
        multi_select_view = FilterMultiSelect(title="Time Series in Container",
                                             height=500, width=400, size=40)
        # Read up on the Presenter
        # - FilterMultiSelect goes into bokeh.layout.columns, but it is passed
        # to SelectorPresenter, what is the role of FilterMultiSelect in the
        # mentioned object ?
        # Seems like a presentor need a "type" of display, here the FMSelect
        multi_select_presenter = SelectorPresenter(name="Ts",
                                                   view=multi_select_view)

        #TODO: Connect all ports for TsSelector
        ts_selector = TsSelector(multi_select_presenter)
        connect_ports(ts_selector.send_event_message,
                      event_messenger.receive_message)
        # Ports to let ts_selector get url and container of dtss
        connect_ports(container_path_input.send_dtss_url, ts_selector.receive_url)
        connect_ports(container_path_input.send_shyft_container, ts_selector.receive_container)

        # Set up TimeSeries Graph
        time_range = sa.UtcPeriod(sa.utctime_now() - sa.Calendar.YEAR,
                                  sa.utctime_now() + sa.Calendar.WEEK)

        time_step_restrictions = [sa.Calendar.HOUR, sa.Calendar.HOUR*3, 
                                  sa.Calendar.DAY, sa.Calendar.WEEK,
                                  sa.Calendar.MONTH, sa.Calendar.QUARTER, sa.Calendar.YEAR]
        ts_viewer = TsViewer(bokeh_document=doc, title="Ts Viewer", 
                             thread_pool_executor=self.thread_pool,
                             init_view_range=time_range, 
                             time_step_restrictions=time_step_restrictions)
        fig = Figure(viewer=ts_viewer, width=1500, height=600, 
                     init_renderers={DiamondScatterRenderer: 6})

        # To view data in table
        table1 = Table(viewer=ts_viewer, width=1500, height=600)

        # Implemented widget to get timeseries from DTSS 
        # and update figure with data
        view_handle = DsView(figure=fig, table=table1)

        # Get dtss url, which dtss to search for data
        connect_ports(container_path_input.send_dtss_url, view_handle.receive_dtss_url)
        
        # Send from TsSelector result (from dtsc.search) to DsView to fetch and
        connect_ports(ts_selector.send_selected_ts, view_handle.receive_time_series)

        # Sendt dtss_url to DsView wich again is used in the DtssTsAdapter to
        # get timeseries  
        connect_ports(container_path_input.send_dtss_url, view_handle.receive_dtss_url)


        dsviewhandle_registry = DsViewHandleRegistryApp()
        # Sendts view_handlers from viewhandler, informs how to get selected
        # data
        connect_ports(view_handle.send_selected, dsviewhandle_registry.receive_ds_view_handles_to_register)
        # Sends the recieved view-handlers to the viewever
        connect_ports(dsviewhandle_registry.send_ds_view_handles_to_add, ts_viewer.receive_ds_view_handles_to_add)
        connect_ports(dsviewhandle_registry.send_ds_view_handles_to_remove,
                      ts_viewer.receive_ds_view_handles_to_remove)

        # Widget to help change LineStyle
        line_style_changer = LineStyleChanger()
        connect_ports(line_style_changer.send_event_message, event_messenger.receive_message)
        connect_ports(view_handle.send_selected, line_style_changer.receive_ds_view_handles_to_add)
        connect_ports(dsviewhandle_registry.send_ds_view_handles_to_add,
                      line_style_changer.receive_ds_view_handles_to_add)
        connect_ports(dsviewhandle_registry.send_ds_view_handles_to_remove,
                      line_style_changer.receive_ds_view_handles_to_remove)


        #TODO: Move ADD/REPLACE and ADD right under list of all TS
        # return bokeh.layouts.column(bokeh.layouts.row(event_messenger.layout),

        return bokeh.layouts.column(bokeh.layouts.row(event_messenger.layout),
                bokeh.layouts.row(
                bokeh.layouts.column(multi_select_view.layout_components['widgets'][1],
                                    multi_select_view.layout_components['widgets'][0],
                bokeh.layouts.row(container_path_input.layout_components["widgets"][0],
                                  container_path_input.layout_components["widgets"][1])),
                fig.layout, ),
                # REPLACE/ADD and Clear TsViewer buttons
                bokeh.layouts.row(dsviewhandle_registry.layout_components["widgets"][1],
                                  dsviewhandle_registry.layout_components["widgets"][0],
                                  line_style_changer.layout, table1.layout))
